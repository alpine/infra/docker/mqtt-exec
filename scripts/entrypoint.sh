#!/bin/sh

: "${MQTT_HOST:=msg.alpinelinux.org}"
: "${MQTT_SSL:=true}"
: "${MQTT_TOPICS:=#}"

[ "$MQTT_DEPS" ] && apk add --no-cache --quiet $MQTT_DEPS

for topic in $MQTT_TOPICS; do
	MQTT_OPTS="$MQTT_OPTS -t $topic"
done

case $MQTT_SSL in
	[Tt][Rr][Uu][Ee])
		MQTT_OPTS="$MQTT_OPTS --port 8883 --cafile /etc/ssl/cert.pem" ;;
esac

exec mqtt-exec -v -h $MQTT_HOST $MQTT_OPTS -- $@
