# mqtt-exec

A simple docker container to listen on mqtt and execute programs.

## Configuration

Environment variables

 - **MQTT_DEPS** (additional dependencies to be installed)
 - **MQTT_TOPICS** (mqtt topics to listen on )
 - **MQTT_HOST** (mqtt host defaults to msg.alpinelinux.org)
 - **MQTT_SSL** (enable mqtt ssl mode)
 - **MQTT_OPTS** (extra options to pass to mqtt-exec)

By default the provided docker-compose will listen on all topics (#) and echo
back the topic and payload. To use your own script you should mount it in the
container and set the command to this script. First argument to the script is
the topic name and second argument is the payload. If you need additional
programs in the container you can install them by adding the package names to
`MQTT_DEPS` and they will be installed on each container run.
