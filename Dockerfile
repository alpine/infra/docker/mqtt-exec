FROM alpine

RUN apk add --no-cache mqtt-exec

COPY scripts /usr/local/bin

ENTRYPOINT [ "entrypoint.sh" ]
